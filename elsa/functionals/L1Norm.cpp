#include "L1Norm.h"
#include "DataContainer.h"
#include "TypeCasts.hpp"
#include "ProximalL1.h"

#include <limits>
#include <stdexcept>

namespace elsa
{
    template <typename data_t>
    L1Norm<data_t>::L1Norm(const DataDescriptor& domainDescriptor)
        : Functional<data_t>(domainDescriptor)
    {
    }

    template <typename data_t>
    data_t L1Norm<data_t>::evaluateImpl(const DataContainer<data_t>& Rx) const
    {
        return Rx.l1Norm();
    }

    template <typename data_t>
    data_t L1Norm<data_t>::convexConjugate(const DataContainer<data_t>& x) const
    {
        auto tmp = ::elsa::cwiseAbs(x).maxElement() - 1;

        if (tmp < 1e-5) {
            return 0;
        } else {
            return std::numeric_limits<data_t>::infinity();
        }
    }

    template <typename data_t>
    bool L1Norm<data_t>::isProxFriendly() const
    {
        return true;
    }

    template <typename data_t>
    DataContainer<data_t> L1Norm<data_t>::proximal(const DataContainer<data_t>& v,
                                                   SelfType_t<data_t> t) const
    {
        DataContainer<data_t> out(v.getDataDescriptor());
        proximal(v, t, out);
        return out;
    }

    template <typename data_t>
    void L1Norm<data_t>::proximal(const DataContainer<data_t>& v, SelfType_t<data_t> t,
                                  DataContainer<data_t>& out) const
    {
        ProximalL1<data_t> prox;
        prox.apply(v, t, out);
    }

    template <typename data_t>
    void L1Norm<data_t>::getGradientImpl(const DataContainer<data_t>&, DataContainer<data_t>&) const
    {
        throw LogicError("L1Norm: not differentiable, so no gradient! (busted!)");
    }

    template <typename data_t>
    LinearOperator<data_t> L1Norm<data_t>::getHessianImpl(const DataContainer<data_t>&) const
    {
        throw LogicError("L1Norm: not differentiable, so no Hessian! (busted!)");
    }

    template <typename data_t>
    L1Norm<data_t>* L1Norm<data_t>::cloneImpl() const
    {
        return new L1Norm(this->getDomainDescriptor());
    }

    template <typename data_t>
    bool L1Norm<data_t>::isEqual(const Functional<data_t>& other) const
    {
        if (!Functional<data_t>::isEqual(other))
            return false;

        return is<L1Norm>(other);
    }

    // ------------------------------------------
    // explicit template instantiation
    template class L1Norm<float>;
    template class L1Norm<double>;
    // template class L1Norm<complex<float>>;
    // template class L1Norm<complex<double>>;

} // namespace elsa
