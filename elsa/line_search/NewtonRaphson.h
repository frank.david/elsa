#pragma once

#include "Functional.h"
#include "LineSearchMethod.h"

namespace elsa
{
    /**
     * @brief Newton-Raphson method for optimizing $\frac{\partial}{\partial \alpha}f(x + \alpha
     * d)$, with $x$ the current guess, $d$ the search direction and $f$ the optimizing function.
     *
     * The Newton-Raphson method optimizes the function above, by iteratively applying
     * $$
     * \alpha = \frac{\langle f^\prime(x), \, d\rangle}{\langle d, \, f^{\prime \prime} d\rangle}
     * $$
     * where $f^{\prime \prime}$ is the Hessian of $f$. Hence, $f$ needs to be
     * twice continuously differentiable.
     *
     * Reference:
     * - An Introduction to the Conjugate Gradient Method Without the Agonizing Pain, by Jonathan
     * Richard Shewchuk https://www.cs.cmu.edu/~quake-papers/painless-conjugate-gradient.pdf
     */
    template <class data_t>
    class NewtonRaphson : public LineSearchMethod<data_t>
    {
    public:
        explicit NewtonRaphson(const Functional<data_t>& f, index_t iterations = 5);

        ~NewtonRaphson() override = default;

        data_t solve(DataContainer<data_t> xi, DataContainer<data_t> di) override;

        /// implement the polymorphic comparison operation
        bool isEqual(const LineSearchMethod<data_t>& other) const override;

    private:
        /// implement the polymorphic clone operation
        NewtonRaphson<data_t>* cloneImpl() const override;

        index_t iters_;
    };
} // namespace elsa
