#include <pybind11/pybind11.h>
#
#include <pybind11/complex.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
#include <pybind11/operators.h>
#include <pybind11/numpy.h>

#include "CUDA.h"
#include "Cloneable.h"
#include "Complex.h"
#include "DataContainer.h"
#include "Descriptors/BlockDescriptor.h"
#include "Descriptors/DataDescriptor.h"
#include "Descriptors/DetectorDescriptor.h"
#include "Descriptors/IdenticalBlocksDescriptor.h"
#include "Descriptors/PartitionDescriptor.h"
#include "Descriptors/CurvedDetectorDescriptor.h"
#include "Descriptors/PlanarDetectorDescriptor.h"
#include "Descriptors/XGIDetectorDescriptor.h"
#include "Descriptors/RandomBlocksDescriptor.h"
#include "Descriptors/VolumeDescriptor.h"
#include "Geometry.h"
#include "LinearOperator.h"
#include "StrongTypes.h"
#include "Utilities/FormatConfig.h"

#include "bind_common.h"
#include "elsaDefines.h"
#include "hints/core_hints.cpp"
#include "spdlog/fmt/bundled/format.h"

namespace py = pybind11;

namespace detail
{
    template <class data_t>
    void add_clonable_linear_operator(py::module& m, const char* name)
    {
        using CloneableLOp = elsa::Cloneable<elsa::LinearOperator<data_t>>;
        using ConstLOpRef = const elsa::LinearOperator<data_t>&;

        py::class_<CloneableLOp> op(m, name);
        op.def("__ne__", py::overload_cast<ConstLOpRef>(&CloneableLOp::operator!=, py::const_),
               py::arg("other"));
        op.def("__eq__", py::overload_cast<ConstLOpRef>(&CloneableLOp::operator==, py::const_),
               py::arg("other"));
        op.def("clone", py::overload_cast<>(&CloneableLOp ::clone, py::const_));
    }

    template <class data_t>
    void add_linear_operator(py::module& m, const char* name)
    {
        using LOp = elsa::LinearOperator<data_t>;
        using DcRef = elsa::DataContainer<data_t>&;
        using ConstDcRef = const elsa::DataContainer<data_t>&;

        auto return_move = py::return_value_policy::move;
        auto ref_internal = py::return_value_policy::reference_internal;

        py::class_<LOp, elsa::Cloneable<LOp>> op(m, name);
        op.def("apply", py::overload_cast<ConstDcRef>(&LOp::apply, py::const_), py::arg("x"),
               return_move);
        op.def("apply", py::overload_cast<ConstDcRef, DcRef>(&LOp::apply, py::const_), py::arg("x"),
               py::arg("Ax"));
        op.def("applyAdjoint", py::overload_cast<ConstDcRef>(&LOp::applyAdjoint, py::const_),
               py::arg("y"), return_move);
        op.def("applyAdjoint", py::overload_cast<ConstDcRef, DcRef>(&LOp::applyAdjoint, py::const_),
               py::arg("y"), py::arg("Aty"));
        op.def("set", py::overload_cast<const LOp&>(&LOp::operator=), py::arg("other"),
               ref_internal);
        op.def("getDomainDescriptor", py::overload_cast<>(&LOp::getDomainDescriptor, py::const_),
               ref_internal);
        op.def("getRangeDescriptor", py::overload_cast<>(&LOp::getRangeDescriptor, py::const_),
               ref_internal);
        op.def(py::init<const elsa::DataDescriptor&, const elsa::DataDescriptor&>(),
               py::arg("domainDescriptor"), py::arg("rangeDescriptor"));
        op.def(py::init<const elsa::LinearOperator<data_t>&>(), py::arg("other"));

        elsa::LinearOperatorHints<data_t>::addCustomMethods(op);
    }

    namespace detail
    {
        elsa::IndexVector_t from_numpy_shape(py::ssize_t ndim, const py::ssize_t* npshape)
        {
            elsa::IndexVector_t shape(ndim);

            // Our interpretaion of shape is different compared to the on
            // by NumPy. We kind of interpret it like (x1, x2, x3, ...), however,
            // NumPy does (..., x3, x2, x1). E.g. in the 2D case, NumPy
            // stores (rows, cols), however, we would store (cols, rows). But
            // we still are both row-major, so we can still just use the pointer
            for (elsa::index_t i = 0; i < ndim; i++) {
                shape[i] = npshape[ndim - 1 - i];
            }

            return shape;
        }

        elsa::IndexVector_t from_numpy_strides(py::ssize_t ndim, const py::ssize_t* npstrides,
                                               std::size_t bytesize)
        {
            elsa::IndexVector_t strides(ndim);

            // Our interpretaion of shape is different compared to the on
            // by NumPy. We kind of interpret it like (x1, x2, x3, ...), however,
            // NumPy does (..., x3, x2, x1). E.g. in the 2D case, NumPy
            // stores (rows, cols), however, we would store (cols, rows). But
            // we still are both row-major, so we can still just use the pointer
            for (elsa::index_t i = 0; i < ndim; i++) {
                strides[i] = npstrides[ndim - 1 - i] / bytesize;
            }

            return strides;
        }

        elsa::IndexVector_t to_numpy_shape(const elsa::IndexVector_t& shape)
        {
            return shape.reverse();
        }

        elsa::IndexVector_t to_numpy_strides(const elsa::IndexVector_t& strides,
                                             std::size_t bytesize)
        {
            return strides.reverse() * bytesize;
        }

        template <class data_t, class T, class... options>
        void exposeBufferInfo(py::class_<T, options...>& c)
        {
            using Map = Eigen::Map<const elsa::Vector_t<data_t>>;

            c.def(
                py::init([](py::array_t<data_t, py::array::c_style | py::array::forcecast> array) {
                    auto shape = from_numpy_shape(array.ndim(), array.shape());
                    auto map = Map(array.data(), shape.prod());

                    elsa::VolumeDescriptor dd{shape};

                    return std::make_unique<elsa::DataContainer<data_t>>(dd, map);
                }));
            c.def(
                py::init([](py::array_t<elsa::GetFloatingPointType_t<data_t>> npspacing,
                            py::array_t<data_t, py::array::c_style | py::array::forcecast> array) {
                    auto shape = from_numpy_shape(array.ndim(), array.shape());

                    auto map = Map(array.data(), shape.prod());

                    // TODO: do we assume spacing is in our order?
                    elsa::RealVector_t spacing(array.ndim());
                    for (elsa::index_t i = 0; i < spacing.size(); i++) {
                        spacing[i] = npspacing.data()[i];
                    }

                    elsa::VolumeDescriptor dd{shape, spacing};

                    return std::make_unique<elsa::DataContainer<data_t>>(dd, map);
                }));
            c.def(py::init([](py::array_t<data_t, py::array::c_style | py::array::forcecast> array,
                              const elsa::DataDescriptor& desc) {
                elsa::IndexVector_t shape = desc.getNumberOfCoefficientsPerDimension();

                auto map = Map(array.data(), shape.prod());

                return std::make_unique<elsa::DataContainer<data_t>>(desc, map);
            }));
            c.def_buffer([](elsa::DataContainer<data_t>& m) {
                const auto& desc = m.getDataDescriptor();
                auto shape = to_numpy_shape(desc.getNumberOfCoefficientsPerDimension());
                auto strides =
                    to_numpy_strides(desc.getProductOfCoefficientsPerDimension(), sizeof(data_t));

                return py::buffer_info(
                    &m[0], sizeof(data_t), py::format_descriptor<data_t>::format(),
                    m.getDataDescriptor().getNumberOfDimensions(), shape, strides);
            });
        }
    } // namespace detail

    template <class data_t>
    void add_data_container(py::module& m, const char* name)
    {
        using Dc = elsa::DataContainer<data_t>;
        using ConstDcRef = const Dc&;
        using IndexVec = Eigen::Matrix<long, Eigen::Dynamic, 1>;

        py::class_<Dc> dc(m, name, py::buffer_protocol());

        const auto ref_internal = py::return_value_policy::reference_internal;
        const auto move = py::return_value_policy::move;

        dc.def(
            "__setitem__", [](Dc& self, const Dc& other) { self = other; },
            "Set the DataContainer from another");
        dc.def(
            "__setitem__", [](Dc& self, data_t scalar) { self = scalar; },
            "Fill the DataContainer with a scalar");

        // Element Access
        dc.def(
            "__setitem__", [](Dc& self, elsa::index_t idx, data_t val) { self[idx] = val; },
            "Set element at given index in DataContainer");
        dc.def(
            "__getitem__", [](Dc& self, elsa::index_t idx) { return self[idx]; },
            "Get element at given index");

        // Coordinate access
        dc.def(
            "__setitem__",
            [](Dc& self, IndexVec idx, data_t val) {
                if (self.getDataDescriptor().getNumberOfDimensions() != idx.size()) {
                    throw py::index_error(fmt::format(
                        "too many indices for array: array is {}-dimensional, but {} were indexed",
                        self.getDataDescriptor().getNumberOfDimensions(), idx.size()));
                }
                self(idx.reverse()) = val;
            },
            "Set DataContainer element with coordinate index. This follows the NumPy convention of "
            "access.");
        dc.def(
            "__getitem__",
            [](Dc& self, IndexVec idx) {
                if (self.getDataDescriptor().getNumberOfDimensions() != idx.size()) {
                    throw py::index_error(fmt::format(
                        "too many indices for array: array is {}-dimensional, but {} were indexed",
                        self.getDataDescriptor().getNumberOfDimensions(), idx.size()));
                }
                return self(idx.reverse());
            },
            "Access DataContainer element with coordinate index. This "
            "follows the NumPy convention of access.");

        // Equality comparison
        dc.def(py::self == py::self);
        dc.def(py::self != py::self);

        // Inplace vector operations
        dc.def("set", py::overload_cast<ConstDcRef>(&Dc::operator=), py::arg("other"),
               ref_internal);
        dc.def(py::self += py::self);
        dc.def(py::self -= py::self);
        dc.def(py::self *= py::self);
        dc.def(py::self /= py::self);

        // Inplace scalar operations
        dc.def("set", py::overload_cast<data_t>(&Dc::operator=), py::arg("scalar"), ref_internal);
        dc.def(py::self += data_t());
        dc.def(py::self -= data_t());
        dc.def(py::self *= data_t());
        dc.def(py::self /= data_t());

        // Unary operations
        dc.def(+py::self);
        dc.def(-py::self);

        // Binary operations
        dc.def(py::self + py::self);
        dc.def(py::self - py::self);
        dc.def(py::self * py::self);
        dc.def(py::self / py::self);

        dc.def(py::self + data_t());
        dc.def(py::self - data_t());
        dc.def(py::self * data_t());
        dc.def(py::self / data_t());

        dc.def(data_t() + py::self);
        dc.def(data_t() - py::self);
        dc.def(data_t() * py::self);
        dc.def(data_t() / py::self);

        // Block operations
        dc.def("viewAs", py::overload_cast<const elsa::DataDescriptor&>(&Dc::viewAs),
               py::arg("descriptor"), move);
        dc.def("viewAs", py::overload_cast<const elsa::DataDescriptor&>(&Dc::viewAs, py::const_),
               py::arg("descriptor"), move);
        dc.def("getBlock", py::overload_cast<long>(&Dc::getBlock), py::arg("i"), move);
        dc.def("getBlock", py::overload_cast<long>(&Dc::getBlock, py::const_), py::arg("i"), move);
        dc.def("slice", py::overload_cast<long>(&Dc::slice), py::arg("i"), move);
        dc.def("slice", py::overload_cast<long>(&Dc::slice, py::const_), py::arg("i"), move);

        dc.def("getDataDescriptor", py::overload_cast<>(&Dc::getDataDescriptor, py::const_),
               ref_internal);

        dc.def("at", py::overload_cast<const IndexVec&>(&Dc::at, py::const_),
               py::arg("coordinate"));
        dc.def("getSize", py::overload_cast<>(&Dc::getSize, py::const_));

        dc.def("dot", py::overload_cast<ConstDcRef>(&Dc::dot, py::const_), py::arg("other"));
        dc.def("l1Norm", py::overload_cast<>(&Dc::l1Norm, py::const_));
        dc.def("l2Norm", py::overload_cast<>(&Dc::l2Norm, py::const_));
        dc.def("lInfNorm", py::overload_cast<>(&Dc::lInfNorm, py::const_));
        dc.def("maxElement", py::overload_cast<>(&Dc::maxElement, py::const_));
        dc.def("minElement", py::overload_cast<>(&Dc::minElement, py::const_));
        dc.def("squaredL2Norm", py::overload_cast<>(&Dc::squaredL2Norm, py::const_));
        dc.def("sum", py::overload_cast<>(&Dc::sum, py::const_));
        dc.def("l0PseudoNorm", py::overload_cast<>(&Dc::l0PseudoNorm, py::const_));

        dc.def("fft", py::overload_cast<elsa::FFTNorm, elsa::FFTPolicy>(&Dc::fft), py::arg("norm"),
               py::arg("policy") = elsa::FFTPolicy::AUTO);
        dc.def("ifft", py::overload_cast<elsa::FFTNorm, elsa::FFTPolicy>(&Dc::ifft),
               py::arg("norm"), py::arg("policy") = elsa::FFTPolicy::AUTO);

        using ostream = std::basic_ostream<char, std::char_traits<char>>;
        dc.def("format", py::overload_cast<ostream&, elsa::format_config>(&Dc::format, py::const_),
               py::arg("os"), py::arg("cfg") = elsa::format_config{});

        using Vector = Eigen::Matrix<data_t, Eigen::Dynamic, 1>;
        using ConstDescRef = const elsa::DataDescriptor&;

        dc.def(py::init<ConstDcRef>(), py::arg("other"));
        dc.def(py::init<ConstDescRef, const Vector&>(), py::arg("dataDescriptor"), py::arg("data"));
        dc.def(py::init<ConstDescRef>(), py::arg("dataDescriptor"));

        // Print to standard out
        dc.def("__str__", [](Dc& self) {
            std::stringstream os;
            self.format(os);
            return os.str();
        });

        // Print short summary of DC as representation
        dc.def("__repr__", [](Dc& self) {
            std::stringstream os;
            auto dims = self.getDataDescriptor().getNumberOfDimensions();
            auto shape = self.getDataDescriptor().getNumberOfCoefficientsPerDimension();
            auto ptr = self.storage().data().get();
            os << "DataContainer<dims=" << dims << ", shape=(" << shape.transpose()
               << "), data=" << fmt::ptr(ptr) << ">";
            return os.str();
        });

        // Add some common NumPy-like properties
        dc.def_property_readonly("shape", [](Dc& self) {
            elsa::IndexVector_t tmp =
                self.getDataDescriptor().getNumberOfCoefficientsPerDimension().reverse();
            return py::tuple(py::cast(tmp));
        });

        dc.def_property_readonly("strides", [](Dc& self) {
            return elsa::IndexVector_t(
                self.getDataDescriptor().getProductOfCoefficientsPerDimension().reverse()
                * sizeof(data_t));
        });

        dc.def_property_readonly(
            "ndim", [](Dc& self) { return self.getDataDescriptor().getNumberOfDimensions(); });

        dc.def_property_readonly("size", [](Dc& self) { return self.getSize(); });

        dc.def_property_readonly("itemsize", [](Dc&) { return sizeof(data_t); });

        dc.def_property_readonly("nbytes",
                                 [](Dc& self) { return self.getSize() * sizeof(data_t); });

        dc.def_property_readonly("dtype",
                                 [](Dc&) { return py::format_descriptor<data_t>::format(); });

        detail::exposeBufferInfo<data_t>(dc);
    }
} // namespace detail

void add_linear_operator(py::module& m)
{
    detail::add_clonable_linear_operator<float>(m, "CloneableLinearOperatorf");
    detail::add_linear_operator<float>(m, "LinearOperatorf");
    m.attr("LinearOperator") = m.attr("LinearOperatorf");

    detail::add_clonable_linear_operator<thrust::complex<float>>(m, "CloneableLinearOperatorcf");
    detail::add_linear_operator<thrust::complex<float>>(m, "LinearOperatorcf");

    detail::add_clonable_linear_operator<double>(m, "CloneableLinearOperatord");
    detail::add_linear_operator<double>(m, "LinearOperatord");

    detail::add_clonable_linear_operator<thrust::complex<double>>(m, "CloneableLinearOperatorcd");
    detail::add_linear_operator<thrust::complex<double>>(m, "LinearOperatorcd");
}

namespace detail
{
    template <class Fn, class... Args>
    void add_fn(py::module& m, std::string name, Fn fn, Args... args)
    {
        m.def(name.c_str(), fn, args...);
    }
} // namespace detail

void add_datacontainer_free_functions(py::module& m)
{
    detail::add_fn(m, "clip", elsa::clip<float>);
    detail::add_fn(m, "clip", elsa::clip<double>);

    detail::add_fn(m, "rfft", elsa::rfft<float>, py::arg("dc"), py::arg("norm"),
                   py::arg("policy") = elsa::FFTPolicy::AUTO);
    detail::add_fn(m, "rfft", elsa::rfft<double>, py::arg("dc"), py::arg("norm"),
                   py::arg("policy") = elsa::FFTPolicy::AUTO);
    detail::add_fn(m, "irfft", elsa::irfft<float>, py::arg("dc"), py::arg("norm"),
                   py::arg("policy") = elsa::FFTPolicy::AUTO);
    detail::add_fn(m, "irfft", elsa::irfft<double>, py::arg("dc"), py::arg("norm"),
                   py::arg("policy") = elsa::FFTPolicy::AUTO);

#define ELSA_ADD_FREE_FUNCTION_ALL_TYPES(name)                  \
    detail::add_fn(m, #name, elsa::name<elsa::index_t>);        \
    detail::add_fn(m, #name, elsa::name<float>);                \
    detail::add_fn(m, #name, elsa::name<double>);               \
    detail::add_fn(m, #name, elsa::name<elsa::complex<float>>); \
    detail::add_fn(m, #name, elsa::name<elsa::complex<double>>);

    ELSA_ADD_FREE_FUNCTION_ALL_TYPES(minimum)
    ELSA_ADD_FREE_FUNCTION_ALL_TYPES(maximum)
    ELSA_ADD_FREE_FUNCTION_ALL_TYPES(real)
    ELSA_ADD_FREE_FUNCTION_ALL_TYPES(imag)
    ELSA_ADD_FREE_FUNCTION_ALL_TYPES(cwiseAbs)
    ELSA_ADD_FREE_FUNCTION_ALL_TYPES(square)
    ELSA_ADD_FREE_FUNCTION_ALL_TYPES(exp)
    ELSA_ADD_FREE_FUNCTION_ALL_TYPES(log)
    ELSA_ADD_FREE_FUNCTION_ALL_TYPES(empty)
    ELSA_ADD_FREE_FUNCTION_ALL_TYPES(emptylike)
    ELSA_ADD_FREE_FUNCTION_ALL_TYPES(zeros)
    ELSA_ADD_FREE_FUNCTION_ALL_TYPES(zeroslike)
    ELSA_ADD_FREE_FUNCTION_ALL_TYPES(ones)
    ELSA_ADD_FREE_FUNCTION_ALL_TYPES(oneslike)
    ELSA_ADD_FREE_FUNCTION_ALL_TYPES(materialize)

#undef ELSA_ADD_FREE_FUNCTION

    /* For an unknown reason, the overload of elsa::sqrt<T> cannot be deduced,
     * so it is made explicit with this lambda. It is unclear, why this issue
     * only arises with sqrt, so in case it comes up again in the future, this
     * macro can be reused. */
#define ELSA_ADD_TRANSFORM_WRAPPED(name, type) \
    m.def(#name, [](const elsa::DataContainer<type>& dc) { return elsa::name<>(dc); });

    ELSA_ADD_TRANSFORM_WRAPPED(sqrt, elsa::index_t)
    ELSA_ADD_TRANSFORM_WRAPPED(sqrt, float)
    ELSA_ADD_TRANSFORM_WRAPPED(sqrt, double)
    ELSA_ADD_TRANSFORM_WRAPPED(sqrt, elsa::complex<float>)
    ELSA_ADD_TRANSFORM_WRAPPED(sqrt, elsa::complex<double>)

#undef ELSA_ADD_TRANSFORM_WRAPPED

#define ELSA_ADD_FREE_FUNCTIONS(name)                                      \
    m.def(#name, elsa::name<elsa::index_t, elsa::index_t>);                \
    m.def(#name, elsa::name<elsa::index_t, float>);                        \
    m.def(#name, elsa::name<elsa::index_t, double>);                       \
    m.def(#name, elsa::name<elsa::index_t, elsa::complex<float>>);         \
    m.def(#name, elsa::name<elsa::index_t, elsa::complex<double>>);        \
    m.def(#name, elsa::name<float, float>);                                \
    m.def(#name, elsa::name<float, double>);                               \
    m.def(#name, elsa::name<float, elsa::complex<float>>);                 \
    m.def(#name, elsa::name<float, elsa::complex<double>>);                \
    m.def(#name, elsa::name<double, double>);                              \
    m.def(#name, elsa::name<double, elsa::complex<float>>);                \
    m.def(#name, elsa::name<double, elsa::complex<double>>);               \
    m.def(#name, elsa::name<elsa::complex<float>, elsa::complex<float>>);  \
    m.def(#name, elsa::name<elsa::complex<float>, elsa::complex<double>>); \
    m.def(#name, elsa::name<elsa::complex<double>, elsa::complex<double>>);

    ELSA_ADD_FREE_FUNCTIONS(cwiseMin)
    ELSA_ADD_FREE_FUNCTIONS(cwiseMax)

#undef ELSA_ADD_FREE_FUNCTIONS
}

void add_data_container(py::module& m)
{
    detail::add_data_container<float>(m, "DataContainerf");
    m.attr("DataContainer") = m.attr("DataContainerf");

    detail::add_data_container<double>(m, "DataContainerd");
    detail::add_data_container<thrust::complex<float>>(m, "DataContainercf");
    detail::add_data_container<thrust::complex<double>>(m, "DataContainercd");
    detail::add_data_container<long>(m, "DataContainerl");

    add_datacontainer_free_functions(m);
}

void add_definitions_pyelsa_core(py::module& m)
{
    py::enum_<elsa::FFTNorm>(m, "FFTNorm")
        .value("BACKWARD", elsa::FFTNorm::BACKWARD)
        .value("FORWARD", elsa::FFTNorm::FORWARD)
        .value("ORTHO", elsa::FFTNorm::ORTHO);

    py::enum_<elsa::FFTPolicy>(m, "FFTPolicy")
        .value("AUTO", elsa::FFTPolicy::AUTO)
        .value("DEVICE", elsa::FFTPolicy::DEVICE)
        .value("HOST", elsa::FFTPolicy::HOST);

    using DD = elsa::DataDescriptor;
    py::class_<elsa::Cloneable<DD>> CloneableDataDescriptor(m, "CloneableDataDescriptor");
    CloneableDataDescriptor
        .def("__ne__", py::overload_cast<const DD&>(&elsa::Cloneable<DD>::operator!=, py::const_),
             py::arg("other"))
        .def("__eq__", py::overload_cast<const DD&>(&elsa::Cloneable<DD>::operator==, py::const_),
             py::arg("other"))
        .def("clone", py::overload_cast<>(&elsa::Cloneable<DD>::clone, py::const_));

    using IndexVec = elsa::IndexVector_t;
    const auto move = py::return_value_policy::move;
    py::class_<DD, elsa::Cloneable<DD>> dd(m, "DataDescriptor");
    dd.def("getLocationOfOrigin", py::overload_cast<>(&DD::getLocationOfOrigin, py::const_), move);
    dd.def("getSpacingPerDimension", py::overload_cast<>(&DD::getSpacingPerDimension, py::const_),
           move);
    dd.def("getCoordinateFromIndex",
           py::overload_cast<long>(&DD::getCoordinateFromIndex, py::const_), py::arg("index"),
           move);
    dd.def("getNumberOfCoefficientsPerDimension",
           py::overload_cast<>(&DD::getNumberOfCoefficientsPerDimension, py::const_), move);
    dd.def("getIndexFromCoordinate",
           py::overload_cast<const IndexVec&>(&DD::getIndexFromCoordinate, py::const_),
           py::arg("coordinate"));
    dd.def("getNumberOfCoefficients",
           py::overload_cast<>(&DD::getNumberOfCoefficients, py::const_));
    dd.def("getNumberOfDimensions", &DD::getNumberOfDimensions);
    dd.def("element", &DD::element<elsa::index_t>);
    dd.def("element", &DD::element<float>);
    dd.def("element", &DD::element<double>);
    dd.def("element", &DD::element<elsa::complex<float>>);
    dd.def("element", &DD::element<elsa::complex<double>>);

    py::class_<elsa::format_config> format_config(m, "format_config");
    format_config.def(py::init<const elsa::format_config&>());

    add_data_container(m);
    add_linear_operator(m);

    py::class_<elsa::VolumeDescriptor, DD> VolumeDescriptor(m, "VolumeDescriptor");
    VolumeDescriptor.def(py::init<IndexVec>(), py::arg("numberOfCoefficientsPerDimension"))
        .def(py::init<IndexVec, elsa::RealVector_t>(), py::arg("numberOfCoefficientsPerDimension"),
             py::arg("spacingPerDimension"))
        .def(py::init<std::initializer_list<long>>(), py::arg("numberOfCoefficientsPerDimension"))
        .def(py::init<std::initializer_list<long>, std::initializer_list<float>>(),
             py::arg("numberOfCoefficientsPerDimension"), py::arg("spacingPerDimension"))
        .def(py::init<const elsa::VolumeDescriptor&>());

    using BlockDesc = elsa::BlockDescriptor;
    py::class_<BlockDesc, DD> blockDesc(m, "BlockDescriptor");
    blockDesc.def("getDescriptorOfBlock",
                  py::overload_cast<long>(&BlockDesc::getDescriptorOfBlock, py::const_),
                  py::arg("i"), py::return_value_policy::reference_internal);
    blockDesc.def("getOffsetOfBlock",
                  py::overload_cast<long>(&BlockDesc::getOffsetOfBlock, py::const_), py::arg("i"));
    blockDesc.def("getNumberOfBlocks",
                  py::overload_cast<>(&BlockDesc::getNumberOfBlocks, py::const_));

    using IdBlockDesc = elsa::IdenticalBlocksDescriptor;
    py::class_<IdBlockDesc, BlockDesc> idBlockDesc(m, "IdenticalBlocksDescriptor");
    idBlockDesc.def("getDescriptorOfBlock",
                    py::overload_cast<long>(&IdBlockDesc::getDescriptorOfBlock, py::const_),
                    py::arg("i"), py::return_value_policy::reference_internal);
    idBlockDesc.def("getOffsetOfBlock",
                    py::overload_cast<long>(&IdBlockDesc::getOffsetOfBlock, py::const_),
                    py::arg("i"));
    idBlockDesc.def(
        "getNumberOfBlocks",
        py::overload_cast<>(&elsa::IdenticalBlocksDescriptor::getNumberOfBlocks, py::const_));
    idBlockDesc.def(py::init<long, const elsa::DataDescriptor&>(), py::arg("numberOfBlocks"),
                    py::arg("dataDescriptor"));

    using PartDesc = elsa::PartitionDescriptor;
    py::class_<PartDesc, BlockDesc> partDesc(m, "PartitionDescriptor");
    partDesc
        .def("getDescriptorOfBlock",
             py::overload_cast<long>(&PartDesc::getDescriptorOfBlock, py::const_), py::arg("i"),
             py::return_value_policy::reference_internal)
        .def("getOffsetOfBlock", py::overload_cast<long>(&PartDesc::getOffsetOfBlock, py::const_),
             py::arg("i"))
        .def("getNumberOfBlocks", py::overload_cast<>(&PartDesc::getNumberOfBlocks, py::const_))
        .def(py::init<const DD&, elsa::IndexVector_t>(), py::arg("dataDescriptor"),
             py::arg("slicesInBlock"))
        .def(py::init<const DD&, long>(), py::arg("dataDescriptor"), py::arg("numberOfBlocks"));

    using RandBlockDesc = elsa::RandomBlocksDescriptor;
    py::class_<RandBlockDesc, BlockDesc> randBlockDesc(m, "RandomBlocksDescriptor");
    randBlockDesc
        .def("getDescriptorOfBlock",
             py::overload_cast<long>(&RandBlockDesc::getDescriptorOfBlock, py::const_),
             py::arg("i"), py::return_value_policy::reference_internal)
        .def("getOffsetOfBlock",
             py::overload_cast<long>(&RandBlockDesc::getOffsetOfBlock, py::const_), py::arg("i"))
        .def("getNumberOfBlocks",
             py::overload_cast<>(&RandBlockDesc::getNumberOfBlocks, py::const_));

    elsa::RandomBlocksDescriptorHints::addCustomMethods(randBlockDesc);

    py::class_<elsa::Geometry> Geometry(m, "Geometry");
    Geometry.def("__eq__", &elsa::Geometry::operator==, py::arg("other"))
        .def("getInverseProjectionMatrix", &elsa::Geometry::getInverseProjectionMatrix,
             py::return_value_policy::reference_internal)
        .def("getProjectionMatrix", &elsa::Geometry::getProjectionMatrix,
             py::return_value_policy::reference_internal)
        .def("getRotationMatrix", &elsa::Geometry::getRotationMatrix,
             py::return_value_policy::reference_internal)
        .def("getCameraCenter", &elsa::Geometry::getCameraCenter,
             py::return_value_policy::reference_internal)
        .def(py::init<const elsa::Geometry&>())
        .def(py::init<float, float, const elsa::DataDescriptor&, const elsa::DataDescriptor&,
                      const Eigen::Matrix<float, -1, -1, 0, -1, -1>&, float, float, float, float,
                      float>(),
             py::arg("sourceToCenterOfRotation"), py::arg("centerOfRotationToDetector"),
             py::arg("volumeDescriptor"), py::arg("sinoDescriptor"), py::arg("R"),
             py::arg("px") = static_cast<float>(0.000000e+00),
             py::arg("py") = static_cast<float>(0.000000e+00),
             py::arg("centerOfRotationOffsetX") = static_cast<float>(0.000000e+00),
             py::arg("centerOfRotationOffsetY") = static_cast<float>(0.000000e+00),
             py::arg("centerOfRotationOffsetZ") = static_cast<float>(0.000000e+00));

    py::class_<elsa::DetectorDescriptor, elsa::DataDescriptor> DetectorDescriptor(
        m, "DetectorDescriptor");
    DetectorDescriptor
        .def("computeRayFromDetectorCoord",
             py::overload_cast<const elsa::index_t>(
                 &elsa::DetectorDescriptor::computeRayFromDetectorCoord, py::const_),
             py::arg("coord"), py::return_value_policy::move)
        .def("computeRayFromDetectorCoord",
             py::overload_cast<const elsa::IndexVector_t>(
                 &elsa::DetectorDescriptor::computeRayFromDetectorCoord, py::const_),
             py::arg("detectorIndex"), py::return_value_policy::move)
        .def("computeRayFromDetectorCoord",
             py::overload_cast<const elsa::RealVector_t&, elsa::index_t>(
                 &elsa::DetectorDescriptor::computeRayFromDetectorCoord, py::const_),
             py::arg("detectorCoord"), py::arg("poseIndex"), py::return_value_policy::move)
        .def("getGeometryAt", &elsa::DetectorDescriptor::getGeometryAt, py::arg("index"),
             py::return_value_policy::move)
        .def("getGeometry", &elsa::DetectorDescriptor::getGeometry, py::return_value_policy::move)
        .def("getNumberOfGeometryPoses", (long(elsa::DetectorDescriptor::*)() const)(
                                             &elsa::DetectorDescriptor::getNumberOfGeometryPoses));

    py::class_<elsa::PlanarDetectorDescriptor, elsa::DetectorDescriptor> PlanarDetectorDescriptor(
        m, "PlanarDetectorDescriptor");
    PlanarDetectorDescriptor
        // .def("computeRayFromDetectorCoord",
        //      &elsa::PlanarDetectorDescriptor::computeRayFromDetectorCoord, py::arg("coord"),
        //      py::return_value_policy::move)
        // .def("computeRayFromDetectorCoord",
        //      &elsa::PlanarDetectorDescriptor::computeRayFromDetectorCoord,
        //      py::arg("detectorCoord"), py::arg("poseIndex"), py::return_value_policy::move)
        // .def("computeRayFromDetectorCoord",
        //      &elsa::PlanarDetectorDescriptor::computeRayFromDetectorCoord,
        //      py::arg("detectorIndex"), py::return_value_policy::move)
        .def(py::init<const Eigen::Matrix<long, -1, 1, 0, -1, 1>&,
                      const Eigen::Matrix<float, -1, 1, 0, -1, 1>&,
                      const std::vector<elsa::Geometry, std::allocator<elsa::Geometry>>&>(),
             py::arg("numOfCoeffsPerDim"), py::arg("spacingPerDim"), py::arg("geometryList"))
        .def(py::init<const Eigen::Matrix<long, -1, 1, 0, -1, 1>&,
                      const std::vector<elsa::Geometry, std::allocator<elsa::Geometry>>&>(),
             py::arg("numOfCoeffsPerDim"), py::arg("geometryList"))
        .def(py::init<const elsa::PlanarDetectorDescriptor&>());

    py::class_<elsa::CurvedDetectorDescriptor, elsa::DetectorDescriptor> CurvedDetectorDescriptor(
        m, "CurvedDetectorDescriptor");
    CurvedDetectorDescriptor
        // .def("computeRayFromDetectorCoord",
        //      &elsa::CurvedDetectorDescriptor::computeRayFromDetectorCoord, py::arg("coord"),
        //      py::return_value_policy::move)
        // .def("computeRayFromDetectorCoord",
        //      &elsa::CurvedDetectorDescriptor::computeRayFromDetectorCoord,
        //      py::arg("detectorCoord"), py::arg("poseIndex"), py::return_value_policy::move)
        // .def("computeRayFromDetectorCoord",
        //      &elsa::CurvedDetectorDescriptor::computeRayFromDetectorCoord,
        //      py::arg("detectorIndex"), py::return_value_policy::move)
        .def(py::init<const Eigen::Matrix<long, -1, 1, 0, -1, 1>&,
                      const Eigen::Matrix<float, -1, 1, 0, -1, 1>&,
                      const std::vector<elsa::Geometry, std::allocator<elsa::Geometry>>&,
                      elsa::geometry::Radian, float>(),
             py::arg("numOfCoeffsPerDim"), py::arg("spacingPerDim"), py::arg("geometryList"),
             py::arg("angle"), py::arg("s2d"))
        .def(py::init<const Eigen::Matrix<long, -1, 1, 0, -1, 1>&,
                      const std::vector<elsa::Geometry, std::allocator<elsa::Geometry>>&,
                      elsa::geometry::Radian, float>(),
             py::arg("numOfCoeffsPerDim"), py::arg("geometryList"), py::arg("angle"),
             py::arg("s2d"))
        .def(py::init<const elsa::CurvedDetectorDescriptor&>());

    py::class_<elsa::XGIDetectorDescriptor, elsa::DetectorDescriptor> XGIDetectorDescriptor(
        m, "XGIDetectorDescriptor");
    XGIDetectorDescriptor
        // .def("computeRayFromDetectorCoord",
        //      &elsa::XGIDetectorDescriptor::computeRayFromDetectorCoord, py::arg("coord"),
        //      py::return_value_policy::move)
        // .def("computeRayFromDetectorCoord",
        //      &elsa::XGIDetectorDescriptor::computeRayFromDetectorCoord, py::arg("detectorCoord"),
        //      py::arg("poseIndex"), py::return_value_policy::move)
        // .def("computeRayFromDetectorCoord",
        //      &elsa::XGIDetectorDescriptor::computeRayFromDetectorCoord, py::arg("detectorIndex"),
        //      py::return_value_policy::move)
        .def(py::init<const Eigen::Matrix<long, -1, 1, 0, -1, 1>&,
                      const Eigen::Matrix<float, -1, 1, 0, -1, 1>&,
                      const std::vector<elsa::Geometry, std::allocator<elsa::Geometry>>&,
                      const Eigen::Matrix<elsa::real_t, 3, 1>&, bool>(),
             py::arg("numOfCoeffsPerDim"), py::arg("spacingPerDim"), py::arg("geometryList"),
             py::arg("sensDir"), py::arg("isParallelBeam") = true)
        .def(py::init<const elsa::XGIDetectorDescriptor&>());

#ifdef ELSA_CUDA_PROJECTORS
    py::module cuda = m.def_submodule("cuda", "CUDA functions wrappers");

    cuda.def("setDevice", &elsa::cuda::setDevice);
    cuda.def("getDeviceCount", &elsa::cuda::getDeviceCount);
#endif

    elsa::CoreHints::addCustomFunctions(m);
}

PYBIND11_MODULE(pyelsa_core, m)
{
    add_definitions_pyelsa_core(m);
}
